var delta = 0
var currentScroll = 0

$(document).ready( function() {
  // menu-toggle
  $('.menu-toggle').click(function() {
    if ($('nav').is(':visible')) {
      $('body').removeClass('noscroll')
      $('.menu-toggle .hamburger-icon').removeClass('active')
      $('nav').slideUp('normal', function() {
        $('header').removeClass('open')
      })
    } else {
      $('body').addClass('noscroll')
      $('.menu-toggle .hamburger-icon').addClass('active')
      $('header').addClass('open')
      $('nav').slideDown()
    }

    // $('header').toggleClass('open')
    // $('nav').slideToggle()
  })
  // smooth scroll
  $('a[href*="#"]').click( function(event) {
  	event.preventDefault()
  	$('html, body').animate({
  		scrollTop: $($(this).attr('href')).offset().top
  	}, 750, 'linear')
  })
})

$(window).scroll( function() {
  var scrollPos = $(window).scrollTop()
  var navHeight = $('header').height()
  // home hero video parallax
  $('.hero .hero-video').css('margin-top', scrollPos * .5)

  // home mission bg parallax
  if ($('.mission').length) {
    var missionPos = $('.mission').offset().top
  }
  $('.mission').css('background-position', 'center ' + (scrollPos - missionPos) * 0.5 + 'px')

  // home menu bg color fade
  var heroHeight = $('.page-template-home .hero').height()
  $('.page-template-home header').css('background-color', 'rgba(08, 31, 45, ' + ((scrollPos - heroHeight) / 300) + ')');

  // menu hide on scroll
  currentScroll = scrollPos
  if (delta > currentScroll || scrollPos < 1) {
    $('header').removeClass("scroll-up")
  } else if (delta < currentScroll) {
    $('header').addClass("scroll-up")
  }
  delta = currentScroll
})
