// Include gulp
var gulp = require('gulp')

// Include plugins
var autoprefixer = require('autoprefixer')
var concat = require('gulp-concat')
var cssnano = require('gulp-cssnano')
var mqpacker = require('css-mqpacker')
var postcss = require('gulp-postcss')
var rename = require('gulp-rename')
var sass = require('gulp-sass')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')

// Compile & Minify Styles
gulp.task('styles', function(){
  return gulp.src('src/styles/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass())
  .pipe(postcss([autoprefixer()]))
  .pipe(postcss([mqpacker()]))
  .pipe(sourcemaps.write())
	.pipe(gulp.dest('bundles'))
  .pipe(rename( function(path) {
    path.basename += ".min";
  }))
  .pipe(cssnano())
  .pipe(gulp.dest('dist'))
})

// Concatenate & Minify JS
gulp.task('scripts', function () {
	return gulp.src('src/scripts/*.js')
		.pipe(gulp.dest('bundles'))
    .pipe(rename( function(path) {
			path.basename += ".min";
		}))
		.pipe(uglify())
		.pipe(gulp.dest('dist'))
})

// Watch Files
gulp.task('watch', function(){
  gulp.watch('src/styles/*.scss', gulp.parallel(['styles']))
  gulp.watch('src/scripts/*.js', gulp.parallel(['scripts']))
})

// Default Task
gulp.task('default', gulp.parallel(['styles', 'scripts']))
