<?php
/*
 * Template Name: Home Page
 * Description: The template to show the main home page.
 */
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render('pages/home.twig', $context);
