<?php
/*
 *
 * Menus
 *
 */
// Note that you only need the arguments and register_nav_menus function here.
// They are hooked into WordPress in functions.php.
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );
