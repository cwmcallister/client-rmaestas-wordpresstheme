<?php
// Reference: wp_register_syle( $handle, $src, $deps, $ver, $media );
	function theme_styles(){
		wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Muli:400,700|Ubuntu', array(), null,'all');
		wp_register_style('master-style', get_template_directory_uri() . '/dist/style.min.css', array('google-fonts'), null,'all');
		wp_register_style('fancybox-style', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css', null, null,'all');

		wp_enqueue_style('google-fonts');
		wp_enqueue_style('master-style');
		wp_enqueue_style('fancybox-style');
	}
	add_action('wp_enqueue_scripts', 'theme_styles');

// Reference: wp_register_script( $handle, $src, $deps, $ver, $in_footer );
function load_js_files() {
	if(!is_admin()){
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, '3.3.1', true);
		wp_register_script('main-script', get_template_directory_uri() . '/dist/scripts.min.js', array('jquery'), null, true);
		wp_register_script('fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array('jquery'), null, true);

		wp_enqueue_script('jquery');
		wp_enqueue_script('main-script');
		wp_enqueue_script('fancybox');

		//If necessary, page specific enqueues
		// if ( is_page('') ) {
		// OR if (is_page_template($templateName)) {

		// }
	}
}
add_action( 'wp_enqueue_scripts', 'load_js_files' );

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
