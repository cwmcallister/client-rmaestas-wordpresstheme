<?php
// Create A Simple Theme Options Panel

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

// Start Class
if ( ! class_exists( 'The_Theme_Options' ) ) {

  class The_Theme_Options {

    /**
    * Start things up
    *
    * @since 1.0.0
    */
    public function __construct() {

      // We only need to register the admin panel on the back-end
      if ( is_admin() ) {
        add_action( 'admin_menu', array( 'The_Theme_Options', 'add_admin_menu' ) );
        add_action( 'admin_init', array( 'The_Theme_Options', 'register_settings' ) );
      }

    }

    /**
    * Returns all theme options
    *
    * @since 1.0.0
    */
    public static function get_theme_options() {
      return get_option( 'theme_options' );
    }

    /**
    * Returns single theme option
    *
    * @since 1.0.0
    */
    public static function get_theme_option( $id ) {
      $options = self::get_theme_options();
      if ( isset( $options[$id] ) ) {
        return $options[$id];
      }
    }

    /**
    * Add sub menu page
    *
    * @since 1.0.0
    */
    public static function add_admin_menu() {
      add_menu_page(
        esc_html__( 'Theme Settings', 'text-domain' ),
        esc_html__( 'Theme Settings', 'text-domain' ),
        'manage_options',
        'theme-settings',
        array( 'The_Theme_Options', 'create_admin_page' )
      );
    }

    /**
    * Register a setting and its sanitization callback.
    *
    * We are only registering 1 setting so we can store all options in a single option as
    * an array. You could, however, register a new setting for each option
    *
    * @since 1.0.0
    */
    public static function register_settings() {
      register_setting( 'theme_options', 'theme_options', array( 'The_Theme_Options', 'sanitize' ) );
    }

    /**
    * Sanitization callback
    *
    * @since 1.0.0
    */
    public static function sanitize( $options ) {

      // If we have options lets sanitize them
      if ( $options ) {

        // Checkbox
        // if ( ! empty( $options['checkbox_example'] ) ) {
        //   $options['checkbox_example'] = 'on';
        // } else {
        //   unset( $options['checkbox_example'] ); // Remove from options if not checked
        // }

        // Input
        if ( ! empty( $options['phone'] ) ) {
          $options['phone'] = sanitize_text_field( $options['phone'] );
        } else {
          unset( $options['phone'] ); // Remove from options if empty
        }

        if ( ! empty( $options['linkedin'] ) ) {
          $options['linkedin'] = sanitize_text_field( $options['linkedin'] );
        } else {
          unset( $options['linkedin'] ); // Remove from options if empty
        }

        if ( ! empty( $options['facebook'] ) ) {
          $options['facebook'] = sanitize_text_field( $options['facebook'] );
        } else {
          unset( $options['facebook'] ); // Remove from options if empty
        }

        if ( ! empty( $options['gtm_id'] ) ) {
          $options['gtm_id'] = sanitize_text_field( $options['gtm_id'] );
        } else {
          unset( $options['gtm_id'] ); // Remove from options if empty
        }

        // Textarea
        // if ( ! empty( $options['textarea_example'] ) ) {
        //   $options['textarea_example'] = sanitize_textarea_field( $options['textarea_example'] );
        // } else {
        //   unset( $options['textarea_example'] );
        // }

        // Select
        // if ( ! empty( $options['select_example'] ) ) {
        //   $options['select_example'] = sanitize_text_field( $options['select_example'] );
        // }

      }

      // Return sanitized options
      return $options;

    }

    /**
    * Settings page output
    *
    * @since 1.0.0
    */
    public static function create_admin_page() { ?>

      <div class="wrap">

        <h1><?php esc_html_e( 'Theme Options', 'text-domain' ); ?></h1>

        <form method="post" action="options.php">

          <?php settings_fields( 'theme_options' ); ?>

          <table class="form-table wpex-custom-admin-login-table">

            <tr valign="top">
              <th scope="row"><?php esc_html_e( 'Phone', 'text-domain' ); ?></th>
              <td>
                <?php $value = self::get_theme_option( 'phone' ); ?>
                <input type="tel" name="theme_options[phone]" value="<?php echo esc_attr( $value ); ?>" placeholder="+1 123 456 7890">
              </td>
            </tr>

            <tr valign="top">
              <th scope="row"><?php esc_html_e( 'LinkedIn', 'text-domain' ); ?></th>
              <td>
                <?php $value = self::get_theme_option( 'linkedin' ); ?>
                <input type="url" name="theme_options[linkedin]" value="<?php echo esc_attr( $value ); ?>">
              </td>
            </tr>

            <tr valign="top">
              <th scope="row"><?php esc_html_e( 'Facebook', 'text-domain' ); ?></th>
              <td>
                <?php $value = self::get_theme_option( 'facebook' ); ?>
                <input type="url" name="theme_options[facebook]" value="<?php echo esc_attr( $value ); ?>">
              </td>
            </tr>

            <tr valign="top">
              <th scope="row"><?php esc_html_e( 'Google Tag Manager ID', 'text-domain' ); ?></th>
              <td>
                <?php $value = self::get_theme_option( 'gtm_id' ); ?>
                <input type="text" name="theme_options[gtm_id]" value="<?php echo esc_attr( $value ); ?>">
              </td>
            </tr>

            <tr valign="top">
              <th scope="row"><?php esc_html_e( 'Tracking Scripts', 'text-domain' ); ?></th>
              <td>
                <?php $value = self::get_theme_option( 'tracking_scripts' ); ?>
                <textarea name="theme_options[tracking_scripts]" rows="10"><?php echo $value; ?></textarea>
              </td>
            </tr>

          </table>

          <?php submit_button(); ?>

        </form>

      </div><!-- .wrap -->
    <?php }

  }
}
new The_Theme_Options();

// Helper function to use in your theme to return a theme option value
function myprefix_get_theme_option( $id = '' ) {
  return The_Theme_Options::get_theme_option( $id );
}
